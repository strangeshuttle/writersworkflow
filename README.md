# WritersWorkflow

Using Git to track fiction and export to docx derived from the William Shunn format.

https://format.ms/story.html

Full documentation available at WritersWorkflow/WritersWorkflow.md.
